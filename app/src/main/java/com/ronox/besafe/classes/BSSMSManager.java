package com.ronox.besafe.classes;


import android.app.PendingIntent;
import android.content.Context;
import android.telephony.SmsManager;

import java.util.ArrayList;

public class BSSMSManager
{
    public static String messageTemplate = "%s is in danger. She needs help !!!";

    public static Error sendSms(Context context)
    {
        Error error = null;

        if (nameAndNumberAreValid(context))
        {
            String name = BSDataPersistenceManager.getSavedName(context);
            String phoneNumber = BSDataPersistenceManager.getSavedPhoneNumber
                    (context);

            String message = String.format(messageTemplate, name);
            sendSms(phoneNumber, message);
        }
        else
        {
            error = new Error("Name or number are not valid");
        }

        return error;
    }

    private static void sendSms(String phonenumber, String message)
    {
        SmsManager manager = SmsManager.getDefault();

        int length = message.length();

        if (length > 160)
        {
            ArrayList<String> messagelist = manager.divideMessage(message);

            manager.sendMultipartTextMessage(phonenumber, null, messagelist,
                    null, null);
        }
        else
        {
            manager.sendTextMessage(phonenumber, null, message, null, null);
        }

    }

    private static boolean nameAndNumberAreValid(Context context)
    {
        String name = BSDataPersistenceManager.getSavedName(context);
        String phoneNumber = BSDataPersistenceManager.getSavedPhoneNumber
                (context);

        return (name.length() > 0) && (phoneNumber.length() > 0);
    }
}
