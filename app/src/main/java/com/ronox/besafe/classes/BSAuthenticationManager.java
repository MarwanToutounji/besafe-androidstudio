package com.ronox.besafe.classes;


import android.content.Context;


public class BSAuthenticationManager
{
    public static boolean isFirstConfiguration(Context context)
    {
        Boolean isFirstConfiguration = BSDataPersistenceManager.getSavedFirstConfiguration(context);
        BSDataPersistenceManager.saveFirstConfiguration(context, false);
        return isFirstConfiguration;
    }

    public static boolean validateFieldsAndSave(Context context, String name, String password, String phoneNumber)
    {
        if (name.length() <= 0 || password.length() <= 0 || phoneNumber.length() <= 0)
        {
            return false;
        }
        else
        {
            BSDataPersistenceManager.saveName(context, name);
            BSDataPersistenceManager.savePhoneNumber(context, phoneNumber);
            BSDataPersistenceManager.savePasword(context, password);
            return true;
        }
    }
}
