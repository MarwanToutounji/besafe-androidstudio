package com.ronox.besafe.classes;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import java.util.HashMap;
import java.util.Set;

public class BSBlueToothManager
{
    //region  S I N G E L T O N   I N S T A N C E
    private static BSBlueToothManager instance = null;
    //region V A R I A B L E S
    private BluetoothAdapter mBluetoothAdapter = null;
    // TODO: Have a global variable that holds the Connected device
    private boolean haveBluetooth = true;
    //endregion

    public BSBlueToothManager(Context context)
    {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null)
        {
            // Device does not support Bluetooth
            haveBluetooth = false;
            new AlertDialog.Builder(context)
                    .setTitle("Game over")
                    .setMessage("You have no Bluetooth Game over")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.cancel();
                        }
                    }).show();
        }
    }
    //endregion

    public static BSBlueToothManager getInstance(Context context)
    {
        if (instance == null)
        {
            instance = new BSBlueToothManager(context);
        }
        return instance;
    }

    /**
     * The Activity calling this method should wait for a result
     *
     * @param activity
     */
    public void enableBluetooth(Activity activity)
    {
        // This will issue a request to enable Bluetooth through the system
        if (!mBluetoothAdapter.isEnabled())
        {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent,
                    BSBlueToothConstants.REQUEST_ENABLE_BLUETOOTH);
        }
    }


    public HashMap<String, String> connectToPairedDevices()
    {
        /**
         * First We check if we have a paired device
         * If not then we try to connect
         */

        HashMap<String, String> devicesHashMaps = null;

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are paired devices
        if (pairedDevices.size() > 0)
        {
            devicesHashMaps = new HashMap<String, String>();

            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices)
            {
                // Add the name and address to a hash to be displayed in a
                // list view
                devicesHashMaps.put(device.getAddress(), device.getName());
            }
        }

        return devicesHashMaps;
    }


    public void discoverDevices()
    {
        mBluetoothAdapter.startDiscovery();
    }


    /**
     * Connect to device
     */
    public void connectToDevice()
    {

    }

}
