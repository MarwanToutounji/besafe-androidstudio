package com.ronox.besafe.classes;

import android.content.Context;
import android.content.SharedPreferences;


public class BSDataPersistenceManager
{

    public enum SavedPreference
    {
        NAME, NUMBER, PASSWORD, FIRST_LAUNCH;
    }

    // Declare private variables
    private static final String sharedPreferencesName = "com.ronox.besafe.BeSafeSharedPreference";
    private static SharedPreferences sharedPreferences = null;

    // Public
    public static void saveName(Context context, String value)
    {
        saveToSharedPreference(context, SavedPreference.NAME, value);
    }

    public static void savePhoneNumber(Context context, String value)
    {
        saveToSharedPreference(context, SavedPreference.NUMBER, value);
    }

    public static void savePasword(Context context, String value)
    {
        saveToSharedPreference(context, SavedPreference.PASSWORD, value);
    }

    public static void saveFirstConfiguration(Context context, Boolean value)
    {
        saveBooleanToSharedPreference(context, SavedPreference.FIRST_LAUNCH, value);
    }

    public static String getSavedName(Context context)
    {
        return getFromSavedPreferences(context, SavedPreference.NAME);
    }

    public static String getSavedPhoneNumber(Context context)
    {
        return getFromSavedPreferences(context, SavedPreference.NUMBER);
    }

    public static String getSavedPassword(Context context)
    {
        return getFromSavedPreferences(context, SavedPreference.NUMBER);
    }

    public static Boolean getSavedFirstConfiguration(Context context)
    {
        return getBooleanFromSavedPreferences(context, SavedPreference.FIRST_LAUNCH);
    }

    // Private
    private static void saveToSharedPreference(Context context,
                                               SavedPreference preferenceName, String value)
    {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(preferenceName.toString(), value);
        editor.commit();
    }

    private static void saveBooleanToSharedPreference(Context context,
                                                      SavedPreference preferenceName, Boolean value)
    {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(preferenceName.toString(), value);
        editor.commit();
    }

    private static String getFromSavedPreferences(Context context,
                                                  SavedPreference preferenceName)
    {
        return getSharedPreferences(context).getString(
                preferenceName.toString(), null);
    }

    private static Boolean getBooleanFromSavedPreferences(Context context,
                                                          SavedPreference preferenceName)
    {
        return getSharedPreferences(context).getBoolean(
                preferenceName.toString(), true);
    }

    // Getter
    private static SharedPreferences getSharedPreferences(Context context)
    {
        if (sharedPreferences == null)
        {
            sharedPreferences = context.getSharedPreferences(
                    sharedPreferencesName, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }
}
