package com.ronox.besafe.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.ronox.besafe.R;
import com.ronox.besafe.classes.BSAuthenticationManager;
import com.ronox.besafe.classes.BSBlueToothConstants;
import com.ronox.besafe.classes.BSBlueToothManager;
import com.ronox.besafe.classes.BSDataPersistenceManager;
import com.ronox.besafe.classes.BSSMSManager;

import java.util.ArrayList;

public class MainActivity extends Activity
{
    EditText nameEditText;
    EditText emergencyPhoneNumberEditText;
    EditText passwordEditText;
    Button buttonSave;
    Button buttonTestSMS;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameEditText = (EditText) findViewById(R.id.editTextFullName);
        emergencyPhoneNumberEditText = (EditText) findViewById(R.id.editTextPhoneNumber);
        passwordEditText = (EditText) findViewById(R.id.editTextPassword);
        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonTestSMS = (Button) findViewById(R.id.buttonTestSMS);

        buttonSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                saveButtonClicked();
            }
        });

        buttonTestSMS.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                sendSMSButtonClicked();
            }
        });

        checkIfFirstLaunch();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == BSBlueToothConstants.REQUEST_ENABLE_BLUETOOTH)
        {
            if (resultCode == RESULT_OK)
            {
                /**
                 * BlueTooth was enabled (GREAT SUCCESS)
                 */
            }
            else
            {
                /**
                 * Bluetoothe wasn't successfuly enabled, or user pressed no
                 */
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //region A C T I O N S
    private void saveButtonClicked()
    {
        hideKeyboard();

        String name = nameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String phoneNumber = emergencyPhoneNumberEditText.getText().toString();

        if (BSAuthenticationManager.validateFieldsAndSave(this, name, password, phoneNumber))
        {
            new AlertDialog.Builder(this)
                    .setTitle("Update information")
                    .setMessage("Saving your information is successful")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
        else
        {
            new AlertDialog.Builder(this)
                    .setTitle("Required fields")
                    .setMessage("Same required fields are missing or invalid please fill them and save again")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.cancel();
                        }
                    })
                    .show();

            // Refill the original information
            fillSavedInformation();
        }
    }

    private void sendSMSButtonClicked()
    {
        BSSMSManager.sendSms(this);
    }



    private void connectDevice()
    {
//        BSBlueToothManager.getInstance(this).
    }
    //endregion


    //region H E L P E R S
    private void checkIfFirstLaunch()
    {
        if (BSAuthenticationManager.isFirstConfiguration(this))
        {
            new AlertDialog.Builder(this)
                    .setTitle("Welcome configure your information")
                    .setMessage("Please fill in the displayed form")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
        else
        {
            fillSavedInformation();
        }
    }

    private void fillSavedInformation()
    {
        String name = BSDataPersistenceManager.getSavedName(this);
        String password = BSDataPersistenceManager.getSavedPhoneNumber(this);
        String phoneNumber = BSDataPersistenceManager.getSavedPassword(this);

        nameEditText.setText(name);
        passwordEditText.setText(password);
        emergencyPhoneNumberEditText.setText(phoneNumber);
    }

    private void hideKeyboard()
    {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(nameEditText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(passwordEditText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(emergencyPhoneNumberEditText.getWindowToken(), 0);
    }
    //endregion
}
